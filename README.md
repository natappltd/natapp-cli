## natapp cli ##
---

The aim of the natapp cli is to help avoid common issues when building mobile applications with ionic-native. Below you can see a list of checks to see what we support


### Commands ###
---

`$ natapp doctor check` | Alias `natapp d c` - by default this will run all checks for both IOS and Android.
Available flags: `--ios` , `--android` (filters the checks to the specified flag)


`$ natapp doctor list` | Alias: `natapp d ls` - lists all the checks that are available.
Available flags: `--ios` , `--android` (filters the list to the specified flag)

For a detailed look at what each command does you can run: `$ natapp doctor <command> -help`


## Contributing ##
---

The checks that are run are stored in the `/checks` folder, Each Check is simply a factory function which should contain the following public information:
- Name of check
- Platform (ios, android, both)
- Test Method
- Solution Method
- Automatic solution(boolean)

Both the test and solution method should return an object detailing the outcome of the check / solution. e.g.:

```JS
return {
    success: true,
    error: null,
    message: 'Successfully instlled Ionic WkWebView Plugin'
}
```

To add a new check simply add a new JS file to `/checks` folder include the above information. Export your factory function and import it within the `/checks/index.js` file.

