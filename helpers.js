'use strict'

const fs = require('fs');
const et = require('elementtree');
const chalk = require('chalk');
const promptly = require('promptly');
const SubElement = et.SubElement;

const PLATFORMS = {
    ANDROID: 'android',
    IOS: 'ios',
    BOTH: 'both'
}

function parsePackageJson(path = null) {
    try {
        const pjsonPath = path || process.cwd() + '/package.json';
        const pjson = fs.readFileSync(pjsonPath, 'utf8');
        return JSON.parse(pjson);
    }
    catch (error) {
        throw new Error(chalk.red('Unable to find package.json'));
    }
}

/**
 * Searches the package.json file for a package (runs relative to where the user called the command)
 * @param {string} packageName Package Name to look for
 * @returns {boolean}
 */
function checkForNpmPackage(packageName) {

    try {
        const pjson = parsePackageJson();
        return (pjson && pjson["dependencies"] && pjson["dependencies"][packageName])
    }
    catch (error) {
        throw new Error(chalk.red(error));
    }
}

function parseConfigXml() {
    const configPath = process.cwd() + '/config.xml';
    try {
        const configString = fs.readFileSync(configPath).toString();
        const parsedConfig = et.parse(configString);
        return parsedConfig;
    } catch (error) {
        throw chalk.red('Unable to find config.xml')
    }
}

function writeVariablesForPlugin(pluginName, variables = []) {

    const config = parseConfigXml();
    const plugins = config.findall('./plugin');

    const thePlugin = plugins.find((plugin) => {
        let name = plugin.get('name');
        return (name === pluginName);
    });


    for (let variable of variables) {
        newVar = SubElement(thePlugin, 'variable');
        newVar.set("name", variable.name);
        newVar.set("value", variable.value);
    }

    config.write('config.xml');
}

/**
 * Prompts the user for text input, returns the input
 */
async function promptForInput(question) {

    function validator(response) {
        return (response && typeof response === 'string') ? response : false
    }

    try {
        return await promptly.prompt(question + ': ', { validator });
    } catch (error) {
        console.error('Invalid input, must be a string');
        return promptForInput(question);
    }
}


/**
 * Finds a config target returns true if the config target has been found and contains a string value, return false if not found or incomplete
 */
function findConfig(configTarget) {

    const platform = parseConfigXml().findall('platform').filter((p) => {
        return p.get('name') === PLATFORMS.IOS
    })[0];

    if (!platform) { return false }

    const configElement = platform.findall('edit-config').filter(c => {
        return c.get('target') === configTarget;
    })[0];

    if (!configElement) { return false }

    const configStringElement = configElement.find('string');

    if (!configStringElement || !configStringElement.text || !configStringElement.text.length) { return false }

    return true;
}


/**
 * Searches the config.xml file for the specified <edit-config> element (will create if not found) and updates the string value to the specified text.
 * @param {string} configTarget config target to update/create
 * @param {string} description usage description
 */
function updateConfigDescription(configTarget, description) {

    const configXml = parseConfigXml();

    const platform = configXml.findall('platform').filter((p) => {
        return p.get('name') === PLATFORMS.IOS
    })[0];

    const configElement = platform.findall('edit-config').filter(c => {
        return c.get('target') === configTarget;
    })[0];

    if (configElement) {
        // target found update its description

        let string = configElement.find('string');

        if (string) {
            string.text = description;
        } else {
            //create a new string
            SubElement(configElement, 'string').text = description;
        }

    } else {

        // no target yet lets create it with a new description
        let newConfigElement = SubElement(platform, 'edit-config', {
            'file': '*-Info.plist',
            'mode': 'merge',
            'target': configTarget
        })
        SubElement(newConfigElement, 'string').text = description;
    }

    //write these changes to the disk
    const newConfig = configXml.write({ indent: 4 });
    fs.writeFileSync(process.cwd() + '/config.xml', newConfig);
}



/**
 * @typedef {Object} ConfigResponse
 * @property {boolean} found - The plugin has been found
 * @property {Array.<string>} missingVariables - The variables missing from our plugin
 * @property {any} variables - The Variables we found
 */

/**
 * Searches the config.xml file for the specified plugin and any variables
 * @param {string} pluginName Plugin Name to find
 * @param {Array.<string>} variables Variables to find for the plugin
 * @returns {ConfigResponse}
 */
function checkForCordovaPlugin(pluginName, variables = []) {

    if (!pluginName) { return console.error('Missing plugin name') }

    try {
        const configXml = parseConfigXml();
        const plugins = configXml.findall('./plugin');

        let thePlugin = plugins.find((plugin) => {
            let name = plugin.get('name');
            return (name === pluginName);
        });

        if (!thePlugin) {
            return {
                found: false,
                missingVariables: []
            }
        }

        if (!variables.length) {
            return {
                found: true,
                missingVariables: []
            }
        }

        const elementVariables = thePlugin.findall('variable');

        const missingVariables = variables.reduce((acc, variable) => {

            if (thePlugin && typeof variable === 'string') {
                const variableFound = elementVariables.find(elemVar => {
                    return elemVar.get('name') === variable;
                })
                if (!variableFound) { acc.push(variable) }
            }
            return acc;
        }, []);

        return {
            found: true,
            missingVariables,
            variables: elementVariables
        }

    } catch (error) {
        throw error;
    }
}

module.exports = {
    parseConfigXml,
    checkForCordovaPlugin,
    writeVariablesForPlugin,
    checkForNpmPackage,
    PLATFORMS,
    updateConfigDescription,
    findConfig,
    promptForInput,
    parsePackageJson
};