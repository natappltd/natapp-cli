const FABRIC_API_KEY_TRUNCATED = 'a11e06230c';
const FABRIC_API_SECRET_TRUNCATED = '2093a52edd';

const AUTHOR = {
    EMAIL: "info@natural-apptitude.co.uk",
    URL: "https://www.natural-apptitude.co.uk/",
    NAME: "Natural Apptitude"
}

module.exports = {
    FABRIC_API_KEY_TRUNCATED,
    FABRIC_API_SECRET_TRUNCATED,
    AUTHOR
}