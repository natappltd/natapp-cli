'use strict';

const coreoDoc = require('commander');
const chalk = require('chalk');
const checks = require('./checks');
const { PLATFORMS } = require('./helpers')

coreoDoc
    .option('--ios', 'Filter list to IOS only')
    .option('--android', 'Filter list to Android only')
    .parse(process.argv);

(function () {

    console.log(chalk.green('Running List'));

    //Next we'll check for options and filter our checks accordingly
    function filteredTests() {
        const options = coreoDoc.opts();

        return checks.filter((check) => {
            if (check().platform === PLATFORMS.BOTH || (options.android && options.ios)) {
                return true;
            } else if (options.ios) {
                return check().platform === PLATFORMS.IOS;
            } else if (options.android) {
                return check().platform === PLATFORMS.ANDROID;
            }
            return true;
        });
    }

    //List all our checks!
    for (let check of filteredTests()) {
        console.log(
            ` - ${chalk.magenta(check().name)} (${chalk.blue(check().platform)})`,
            `\n   ${check().description}`
        );
    }

})();
