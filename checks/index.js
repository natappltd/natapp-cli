// All Check files to be imported here.
const fabricInstalled = require('./fabric-installed');
const wkwebviewInstalled = require('./wkwebview-installed');
const coreoBuildInstalled = require('./coreo-build-installed');
const polyfillsLocations = require('./polyfills-location');
const configIds = require('./config-ids');
const configAuthor = require('./config-author');
const pluginPermissions = require('./plugin-permissions');
const exportCompliance = require('./export-compliance');

module.exports = [
    fabricInstalled,
    wkwebviewInstalled,
    coreoBuildInstalled,
    polyfillsLocations,
    configIds,
    configAuthor,
    pluginPermissions,
    exportCompliance
];