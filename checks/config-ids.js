'use strict';

const { parseConfigXml } = require('../helpers');

function ConfigIds() {

    const ID_PREFIX = 'com.natural_apptitude';
    const IOS_PREFIX = 'com.natural-apptitude';

    function test() {

        const widgetIds = findConfigWidgetVariables();

        const idIsCorrect = widgetIds.id ? widgetIds.id.startsWith(ID_PREFIX) : null;
        const iosIdIsCorrect = widgetIds.iosId ? widgetIds.iosId.startsWith(IOS_PREFIX) : null;

        if (idIsCorrect && iosIdIsCorrect) {
            return {
                success: true,
                error: null,
                message: 'Both the widget id and the ios id start with the correct prefix'
            }

        }

        return {
            success: false,
            error: null,
            message: `Widget id is ${idIsCorrect ? 'correct' : 'incorrect'},\nWidget IOS id is ${iosIdIsCorrect ? 'correct' : 'incorrect'}`
        }

    }

    function guidance() {
        return `Ensure that the widget ID within the config.xml file starts with '${ID_PREFIX}' followed by the project name and that the widget 'ios-CFBundleIdentifier' starts with ${IOS_PREFIX} followed by the same project name as the id`

    }

    return {
        name: 'check-ids',
        description: 'Checks that both config widget id and ios bundle id start with correct prefix.',
        platform: 'both',
        automaticSolution: false,
        test,
        guidance
    }

}


function findConfigWidgetVariables() {
    const configXml = parseConfigXml();
    const widget = configXml.getroot();
    return {
        id: widget.get('id'),
        iosId: widget.get('ios-CFBundleIdentifier')
    }
}


module.exports = ConfigIds;
