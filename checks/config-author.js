'use strict';

const { parseConfigXml } = require('../helpers');
const fs = require('fs');
const { AUTHOR } = require('../config');

function ConfigAuthor() {

    function test() {

        const configXml = parseConfigXml();
        const author = configXml.find('author');
        let messages = [];

        (author.get('email') === AUTHOR.EMAIL)
            ? messages.push('Author email is correct')
            : messages.push('Author email in incorrect');

        (author.get('href') === AUTHOR.URL)
            ? messages.push('Author href is correct')
            : messages.push('Author href is incorrect');

        (author.text === AUTHOR.NAME)
            ? messages.push('Author text is correct')
            : messages.push('Author text is incorrect');

        const message = messages.join('\n')

        if (messages.some(msg => msg.indexOf('incorrect') !== -1)) {
            return {
                success: false,
                error: null,
                message
            }
        }

        return {
            success: true,
            error: null,
            message,
        }

    }



    function solution() {

        try {

            const configXml = parseConfigXml();
            let author = configXml.find('author');

            author.clear();

            author.text = AUTHOR.NAME;
            author.set('href', AUTHOR.URL);
            author.set('email', AUTHOR.EMAIL);

            const newConfigXml = configXml.write({ indent: 4 });

            fs.writeFileSync(process.cwd() + '/config.xml', newConfigXml, 'utf8', err => {
                if (err) throw err;
            })

            return {
                success: true,
                error: null,
                message: 'Updated your config.xml file'
            }

        } catch (error) {

            return {
                succes: false,
                error,
                message: "Unable to update your config.xml there may be additional error output above"
            }
        }
    }


    return {
        name: 'config-author',
        description: 'Checks that the author information detailed in the config.xml is correct',
        platform: 'both',
        automaticSolution: true,
        test,
        solution
    }

}


module.exports = ConfigAuthor;
