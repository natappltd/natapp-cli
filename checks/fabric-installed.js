'use strict';

const { FABRIC_API_KEY_TRUNCATED, FABRIC_API_SECRET_TRUNCATED } = require('../config');
const { checkForCordovaPlugin } = require('../helpers');

const VARIABLE_VALUE_MAP = {
    'FABRIC_API_KEY': FABRIC_API_KEY_TRUNCATED,
    'FABRIC_API_SECRET': FABRIC_API_SECRET_TRUNCATED,
    'FABRIC_AUTO_INIT': 'false'
}

function FabricInstalled() {

    function test() {

        try {

            const fabricPresent = checkForCordovaPlugin('cordova-fabric-plugin', ['FABRIC_API_KEY', 'FABRIC_API_SECRET', 'FABRIC_AUTO_INIT']);

            if (fabricPresent && fabricPresent.found) {

                let variableErrors = [];

                if (fabricPresent.missingVariables.length) {
                    variableErrors.push(`❌ Unable to find the following variables:  ${fabricPresent.missingVariables.join(', ')} `);
                }

                if (fabricPresent.variables && fabricPresent.variables.length) {

                    fabricPresent.variables.forEach(variable => {

                        switch (variable.attrib.name) {

                            case 'FABRIC_API_KEY': {
                                if (!variable.attrib.value || variable.attrib.value.startsWith(VARIABLE_VALUE_MAP.FABRIC_API_KEY) === false) {
                                    variableErrors.push(`❌ Incorrect or missing value for 'FABRIC_API_KEY' `);
                                }
                                break;
                            }

                            case 'FABRIC_API_SECRET': {
                                if (!variable.attrib.value || variable.attrib.value.startsWith(VARIABLE_VALUE_MAP.FABRIC_API_SECRET) === false) {
                                    variableErrors.push(`❌ Incorrect or missing value for 'FABRIC_API_SECRET' `);
                                }
                                break;
                            }

                            case 'FABRIC_AUTO_INIT': {
                                if (!variable.attrib.value || variable.attrib.value !== VARIABLE_VALUE_MAP.FABRIC_AUTO_INIT) {
                                    variableErrors.push(`❌ Incorrect or missing value for 'FABRIC_AUTO_INIT' `);
                                }
                                break;
                            }
                        }
                    })
                }

                if (variableErrors.length) {
                    return {
                        success: false,
                        error: null,
                        message: `✅ Fabric plugin found \n` + variableErrors.join('\n')
                    }
                }

                return {
                    success: true,
                    error: null,
                    message: '✅ Fabric plugin found and correct variables found'
                }


            } else {
                return {
                    success: false,
                    error: null,
                    message: '❌  Plugin not found'
                }
            }

        } catch (error) {
            return {
                success: false,
                error: error,
                message: '',
            }
        }
    }


    function guidance() {
        return `You can run and fill in the correct Fabric Key and Secret in the following command: 'ionic cordova plugin add https://github.com/dpa99c/FabricPlugin.git\#opt_in --variable FABRIC_AUTO_INIT=false --variable FABRIC_API_KEY=xxx --variable FABRIC_API_SECRET=xxx' \nIt's also important to ensure you are manually starting the Fabric plugin on app launch, More info on Opt In: https://github.com/dpa99c/FabricPlugin/tree/opt_in`
    }

    return {
        name: 'fabric-check',
        description: 'Checks that the Fabric plugin is installed correctly along with the correct plugin variables',
        platform: 'both',
        automaticSolution: false,
        test,
        guidance
    }
}

module.exports = FabricInstalled;
