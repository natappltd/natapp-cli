'use strict';

const { checkForCordovaPlugin } = require('../helpers');
const spawn = require('cross-spawn');

function exportCompliancePlugin() {

    function test() {

        try {
            const iosNonExemptEncryptionPresent = checkForCordovaPlugin('cordova-plugin-ios-non-exempt-encryption');

            if (iosNonExemptEncryptionPresent.error) {
                return {
                    success: false,
                    error: webviewPresent.error,
                    message: ''
                }
            }

            if (iosNonExemptEncryptionPresent.found) {
                return {
                    success: true,
                    error: null,
                    message: 'IOS export compliance plugin found'
                }
            }

            if (!iosNonExemptEncryptionPresent.found) {
                return {
                    success: false,
                    error: null,
                    message: 'IOS export compliance plugin not found in config.xml'
                }
            }

        } catch (error) {
            return {
                success: false,
                error: error,
                message: ''
            }
        }
    }

    function solution() {

        return installWebviewPlugin().then(
            (data) => {
                return {
                    success: true,
                    error: null,
                    message: 'Successfully instlled IOS export compliance plugin'
                }
            },
            (error) => {
                return {
                    success: false,
                    error: error,
                    message: ''
                }
            }
        )
    }

    function installWebviewPlugin() {
        return new Promise((resolve, reject) => {

            const childProcess = spawn('ionic',
                [
                    'cordova',
                    'plugin',
                    'add',
                    'cordova-plugin-ios-non-exempt-encryption'
                ]
            )

            childProcess.on('close', (code) => {
                console.log(`Completed command code: ${code}`)
                resolve(code);
            })

            childProcess.on('error', (err) => {
                reject(err);
            });
        })
    }

    return {
        name: 'export-compliance',
        description: 'Ensures that the IOS Export compliance ("ITSAppUsesNonExemptEncryption") plugin is installed',
        platform: 'ios',
        automaticSolution: true,
        test,
        solution
    }
}

module.exports = exportCompliancePlugin;
