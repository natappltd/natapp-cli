'use strict';

const { checkForCordovaPlugin, findConfig, promptForInput, updateConfigDescription } = require('../helpers');

function PluginPermissions() {

    // Details supported plugin checks
    const PLUGIN_MAP = [
        {
            name: 'cordova-plugin-geolocation',
            usageTargets: [
                {
                    target: 'NSLocationWhenInUseUsageDescription',
                    requirement: 'Please write a short but detailed description why geolocation usage is needed'
                }
            ]
        },
        {
            name: 'cordova-plugin-camera',
            usageTargets: [
                {
                    target: 'NSCameraUsageDescription',
                    requirement: 'Please write a short but detailed description why camera uage is needed'
                },
                {
                    target: 'NSPhotoLibraryUsageDescription',
                    requirement: 'Please write a short but detailed description why photo library usage is needed'
                },
                {
                    target: 'NSPhotoLibraryAddUsageDescription',
                    requirement: 'Please write a short but detailed description why saving photos to the photo library is needed'
                }
            ]
        }
    ];


    function test() {

        // loop through the plugins we support, check if they are used within the application. if yes check that the variables and *plist are correctly set.
        let issues = false;
        let messages = PLUGIN_MAP.reduce((acc, pluginCheck) => {

            if (checkForCordovaPlugin(pluginCheck.name).found) {

                console.log(`Found plugin: ${pluginCheck.name}`);
                // plugin found let's check its variables and usage descriptions
                pluginCheck.usageTargets.forEach(ut => {

                    if (findConfig(ut.target)) {
                        acc.push(`Found correct implementation of '${ut.target}'`);
                    } else {
                        issues = true;
                        acc.push(`Could not find or incorrect implementation of '${ut.target}' permission target`);
                    }
                })
            }

            return acc;
        }, []);

        return {
            success: !issues,
            error: null,
            message: messages.join('\n') || 'All permissions look good!'
        }

    }

    async function solution() {

        try {
            for (let pluginCheck of PLUGIN_MAP) {

                if (checkForCordovaPlugin(pluginCheck.name).found) {

                    for (let ut of pluginCheck.usageTargets) {

                        if (findConfig(ut.target) === false) {

                            //Request a permission description from the user
                            let description = await promptForInput(ut.requirement);
                            updateConfigDescription(ut.target, description);
                        }

                    }

                }
            }

            return {
                success: true,
                error: null,
                message: 'Succesfully wrote permission values'
            }

        }
        catch (error) {
            return {
                success: false,
                error,
                message: 'Failed to write permission vlaues'
            }

        }
    }

    return {
        name: 'plugin-permission',
        description: 'Looks for common Cordova plugins and checks the usage variables and ios.plist descriptions are filled in',
        platform: 'both',
        automaticSolution: true,
        test,
        solution,
    }

}

module.exports = PluginPermissions;