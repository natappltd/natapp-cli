'use strict';

const { checkForCordovaPlugin } = require('../helpers');
const spawn = require('cross-spawn');

function wkwebviewInstalled() {

    function test() {

        try {
            const webviewPresent = checkForCordovaPlugin('cordova-plugin-ionic-webview');

            if (webviewPresent.error) {
                return {
                    success: false,
                    error: webviewPresent.error,
                    message: ''
                }
            }

            if (webviewPresent.found) {
                return {
                    success: true,
                    error: null,
                    message: 'Webview plugin found'
                }
            }

            if (!webviewPresent.found) {
                return {
                    success: false,
                    error: null,
                    message: 'Webview plugin not found in config.xml'
                }
            }

        } catch (error) {
            return {
                success: false,
                error: error,
                message: ''
            }
        }
    }

    function solution() {

        return installWebviewPlugin().then(
            (data) => {
                return {
                    success: true,
                    error: null,
                    message: 'Successfully instlled Ionic WkWebView Plugin'
                }
            },
            (error) => {
                return {
                    success: false,
                    error: error,
                    message: ''
                }
            }
        )
    }

    function installWebviewPlugin() {
        return new Promise((resolve, reject) => {

            const childProcess = spawn('ionic',
                [
                    'cordova',
                    'plugin',
                    'add',
                    'cordova-plugin-ionic-webview'
                ]
            )

            childProcess.on('close', (code) => {
                console.log(`Completed command code: ${code}`)
                resolve(code);
            })

            childProcess.on('error', (err) => {
                reject(err);
            });
        })
    }

    return {
        name: 'wkwebview-check',
        description: 'Ensures that the WKWebview plugin is installed for the best experience on IOS',
        platform: 'ios',
        automaticSolution: true,
        test,
        solution
    }
}

module.exports = wkwebviewInstalled;
