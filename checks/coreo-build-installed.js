'use strict';

const spawn = require('cross-spawn');
const { checkForNpmPackage } = require('../helpers');

function coreoBuildInstalled() {

    function test() {

        try {
            const exists = checkForNpmPackage('@coreo/ionic-build-info');

            if (exists) {
                return {
                    success: true,
                    error: null,
                    message: 'Coreo Build info package found, Please make sure you are displaying build information in your application'
                }
            }
            return {
                success: false,
                error: null,
                message: 'Unable to find Coreo build info package'
            }
        }
        catch (error) {
            return {
                success: false,
                error: error,
                message: ''
            }
        }
    }

    function solution() {

        return installBuildInfo().then(
            (data) => {
                return {
                    success: true,
                    error: null,
                    message: 'Successfull Added Coreo build info package, Please make sure to utilise this package in your application'
                }
            }).catch(err => {
                return {
                    success: false,
                    error: err,
                    message: ''
                }
            })
    }


    function installBuildInfo() {
        return new Promise((resolve, reject) => {

            const childProcess = spawn('npm',
                [
                    'i',
                    '@coreo/ionic-build-info'
                ]
            )

            childProcess.on('close', (code) => {
                console.log(`Completed command code: ${code}`)
                resolve(code);
            })

            childProcess.on('error', (err) => {
                reject(err);
            });

        })
    }

    return {
        name: 'coreo-build-check',
        description: 'Checks that coreo-build npm module is installed',
        platform: 'both',
        automaticSolution: true,
        test,
        solution
    }
}

module.exports = coreoBuildInstalled;