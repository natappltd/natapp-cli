'use strict';

const fs = require('fs');
const chalk = require('chalk');

function PolyfillsLocation() {

    function test() {

        const indexPath = process.cwd() + '/src/index.html';
        const indexHtmlFile = fs.readFileSync(indexPath, 'utf8').toString();

        const polyfillsIdx = indexHtmlFile.indexOf('<script src="build/polyfills.js"></script>')
        let cordovaJsIdx = indexHtmlFile.indexOf('<script type="text/javascript" src="cordova.js"></script>');

        if (cordovaJsIdx === -1) {
            cordovaJsIdx = indexHtmlFile.indexOf('<script src="cordova.js"></script>');
        }

        if (polyfillsIdx === -1) {
            return {
                success: false,
                error: null,
                message: 'Unable to find pollyfills script'
            }
        }

        if (polyfillsIdx > cordovaJsIdx) {
            return {
                success: false,
                error: null,
                message: 'Polyfills script should be above the cordova.js script within the index.html file'
            }
        }

        return {
            success: true,
            error: null,
            message: 'Polyfills script is above cordova script within the index.html file ',
        }

    }

    function guidance() {

        return `Move the ${chalk.magenta('<script src="build/polyfills.js"></script>')} to above the cordova.js script in ./src/index.html`

    }

    return {
        name: 'polyfills-location',
        description: 'Checks that the polyfills script is located above the cordova.js script within the index.html of the project',
        platform: 'both',
        automaticSolution: false,
        test,
        guidance
    }
}

module.exports = PolyfillsLocation;
