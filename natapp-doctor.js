#!/usr/bin/env node
'use strict';

const coreoDoc = require('commander');

coreoDoc
    .command('*', ' ')
    .action(userCmnd => {
        const knownCommand = coreoDoc.commands.some(command => {
            return (command._name === userCmnd || command._alias === userCmnd);
        })
        return knownCommand ? null : console.error('Unknown Command');
    })
    .command('check', 'Runs all tests, default runs for both IOS and Android').alias('c')
    .command('list', 'Lists all test').alias('ls')
    .parse(process.argv);

