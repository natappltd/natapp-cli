#!/usr/bin/env node
'use strict';

const coreoDoc = require('commander');
const { parsePackageJson } = require('./helpers');
const pjson = parsePackageJson(__dirname + '/package.json');

process.title = "natapp-cli";

coreoDoc
    .version(pjson["version"], '-v, --version')
    .description('Check for common build issues with Ionic/Cordova ios and android apps')
    .command('*', ' ')
    .action(userCmnd => {
        const knownCommand = coreoDoc.commands.some(command => {
            return (command._name === userCmnd || command._alias === userCmnd);
        })
        return knownCommand ? null : console.error('Unknown Command');
    })
    .command('doctor', 'Run natapp doctor').alias('d')
    .parse(process.argv);

process.on('exit', () => {
    console.log('Exiting...bye!');
})