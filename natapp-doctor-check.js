'use strict';

const coreoDoc = require('commander');
const chalk = require('chalk');
const fs = require('fs');
const promptly = require('promptly');
const checks = require('./checks');
const { PLATFORMS } = require('./helpers')

coreoDoc
    .option('--ios', 'Filter checks to IOS only')
    .option('--android', 'Filter checks to Android only')
    .parse(process.argv);

(async function () {

    console.log(chalk.green('👩‍⚕️ Coreo doctor is checking for issues...'));

    //First make sure the user is running the command in an Ionic project
    try {
        fs.readFileSync(process.cwd() + '/ionic.config.json');
    } catch (error) {
        console.error(`❌  ${chalk.red('Unable to find ionic project, Please re-run the command in the root directory of an ionic project.')} `);
        process.exit();
    }

    //Next we'll check for options and filter our checks accordingly
    function filteredTests() {
        const options = coreoDoc.opts();

        return checks.filter((check) => {
            if (check().platform === PLATFORMS.BOTH || (options.android && options.ios)) {
                return true;
            } else if (options.ios) {
                return check().platform === PLATFORMS.IOS;
            } else if (options.android) {
                return check().platform === PLATFORMS.ANDROID;
            }
            return true;
        });
    }

    async function proceedWithFix() {

        function validator(response) {
            if (response === 'y' || response === 'yes') {
                return true;
            } else if (response === 'n' || response === 'no') {
                return false;
            } else if (response === 'q' || response === 'quit') {
                return process.exit()
            }
            throw new Error(`Response must be 'yes', 'no' or 'quit' `);
        }

        try {
            return await promptly.prompt('Fix now? (yes/no/quit) ', { validator });
        } catch (error) {
            console.error(chalk.red(error));
        }
    }


    //Run all our checks!
    for (let check of filteredTests()) {

        console.log(chalk.blue(`Running: ${check().name} `));

        const test = check().test();

        if (test.error) {
            console.error(chalk.red(test.error));
            console.log('Skipping Fix due to error outputted (details above)');
            continue;
        }

        if (test.message) {
            console.log(test.message);
        }

        if (test.success) {
            continue;
        }

        if (check().automaticSolution) {

            const runSolution = await proceedWithFix();
            if (runSolution) {

                const solution = await check().solution();

                console.log((solution && solution.success) ? solution.message : solution.error);

            } else {
                console.log('Skipped..');
            }

        } else {
            //output steps to help user fix
            console.log(chalk.red(`ACTION REQUIRED:`), check().guidance());
        }
    }
})();
